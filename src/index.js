function reducer(action, state = '') {
  if (action.type === "DIGIT") {
    return state = state + action.payLoad;
  } else if (action.type === "OPERATION") {
    return state = state + action.payLoad;
  } else if (action.type === "CLEAR") {
    return state;
  }
  return state;
}


// создаю переменные для: 
let store;
let firstSaveNum = '';       // сохранения числа до знака операции
let operationSaveNum = '';   // сохранения знака операции
let secondSaveNum = '';      // сохранения числа после знака операции
let equalSaveNum = '';       // сохранения всего уровнения для вывода на экран

// отлавливаем нажатые клавиши по классу по цвету
const display = document.querySelector(".display > input")
const digit = document.querySelectorAll(".black");
const text = document.querySelectorAll(".text");
const operation = document.querySelectorAll(".pink");
const equal = document.querySelectorAll(".orange");

//        START добавление цифр

function addNum(num) {

  if (((0 <= num <= 9) || num === ".") & num !== "C") {
    store = reducer({ type: 'DIGIT', payLoad: num });
    if (text.value !== undefined) {
      text.value = text.value + store;
      store = text.value;
      display.value = store;
      if (operationSaveNum === '') {
        firstSaveNum = text.value;
        store = text.value;
        display.value = store;
        console.log('addNum + firstSaveNum ' + store)
      } else {
        secondSaveNum = text.value;
        store = text.value;
        display.value = store;
        console.log('addNum + secondSaveNum ' + store)
      }

    } else {
      text.value = store;
      store = text.value;
      display.value = store;
    }
  } else {
    store = reducer({ type: "CLEAR" });
    text.value = '';
    firstSaveNum = '';
    operationSaveNum = '';
    secondSaveNum = '';
    console.log('addNum + C')
    store = text.value;
    display.value = store;
  }
  return num
}

digit.forEach(button => {
  button.addEventListener('click', () => {
    addNum(button.value);
    console.log("num = " + button.value + " disp operation = " + store);
  })
})

//      END добавления цифр

//      START операций

function addOperation(operation) {
  if (firstSaveNum !== '') {
    operationSaveNum = text.value;
    text.value = '';
    store = reducer({ type: "OPERATION", payLoad: operation });
    text.value = store;
    operationSaveNum = text.value;
    text.value = '';
    console.log('addOperation ' + store)
  } else {
    text.value = '';
    console.log('addOperation undefined')
  }
  return text.value
}

operation.forEach(button => {
  button.addEventListener('click', () => {
    addOperation(button.value);
  })
})

//      END операций

//      START равно

function equalOperation(eq) {
  if (firstSaveNum !== '' & secondSaveNum !== '') {
    text.value = firstSaveNum + ' ' + operationSaveNum + ' ' + secondSaveNum;
    equalSaveNum = text.value;
    store = text.value;
    equalSaveNum = store
    display.value = store;
    console.log('equalOperation ' + store)
  } else {
    text.value = 'РАНО, начинай сначало'
    store = text.value;
    display.value = store;
    firstSaveNum = '';
    operationSaveNum = '';
    secondSaveNum = '';
    console.log(store)
    text.value = '';
  }
}

equal.forEach(button => {
  button.addEventListener('click', () => {
    equalOperation(button.value);
  })
})

//      END равно
